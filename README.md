# APBD dan IPM Per Kabupaten
[data-goid.rhcloud.com](http://data-goid.rhcloud.com/)

## Sumber Data
1. [Ringkasan APBD Pemerintah Kabupaten Kota Indonesia] (http://data.go.id/dataset/ringkasan-apbd-pemerintah-kabupaten-kota-indonesia)
2. [Indeks Pembangunan Manusia IPM](http://data.go.id/dataset/indeks-pembangunan-manusia-ipm)

## Kenapa?
Ringkasan APBD dipilih setelah melakukan melihat beberapa dataset:
1. Ruang lingkup nasional (minus DKI Jakarta)
2. Data yang cukup besar bila dibandingkan dengan beberapa dataset yang lain.
3. Data yang menarik.
4. IPM dipilih untuk melengkapi informasi Kabupaten/Kota di Indonesia.

## Untuk Apa?
Aplikasi memungkinkan publik untuk melihat APBD dan IPM Kabupaten/Kota dengan cepat dan mudah dipahami. 


## Teknologi
1. Hosting di [Openshift by Red Hat] (https://www.openshift.com/), 
OpenShift Online is Red Hat's next-generation application hosting platform 
that makes it easy to run your web applications in the cloud for free.
[https://www.openshift.com/] (https://www.openshift.com/)
2. Pengolahan dan visualisasi data oleh jQuery,
jQuery is a fast, small, and feature-rich JavaScript library. 
It makes things like HTML document traversal and manipulation, event handling, animation, 
and Ajax much simpler with an easy-to-use API that works across a multitude of browsers. 
[http://www.jquery.com](http://www.jquery.com)
2. Chart/Plot oleh jqPlot,
jqPlot is a plotting and charting plugin for the jQuery Javascript framework. 
jqPlot produces beautiful line, bar and pie charts
[http://www.jqplot.com](http://www.jqplot.com/)
4. UI oleh Twitter Bootstrap 3,
Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, 
mobile first projects on the web.
[http://getbootstrap.com/](http://getbootstrap.com/)
6. Konversi csv ke format json dan web fondasi oleh Zend Framework 1,
Pilihan ZF1 semata-mata karena terpikir tentang kemudahan untuk pengembangan bila diperlukan. 
[http://framework.zend.com](http://framework.zend.com)
7. Managemen Source Code oleh GitLab, 
[https://gitlab.com/](https://gitlab.com/)