<?php

class Goid_Source
{
    public function sourceApbd($file)
    {
        $resp  = array();
        //if(is_readable($file)):
            $fopen = fopen($file, "r");
            $src   = array();
            while($csv = fgetcsv($fopen)):
                $v0 = explode('e+', $csv[9] . 'e+0');
                $fx = pow(10, $v0[1]);
                $v1 = $v0[0] * $fx;
                $src[$csv[1]][$csv[3]][$csv[6]][$csv[7]][$csv[8]][$csv[0]] = $v1;
            endwhile;
            fclose($fopen);
        //endif;

        if(count($src > 1)) $resp['propinsi'] = array_slice($src, 1);
        return $resp;
    }

    //nama_provinsi	kode_kabkota	nama_kabkota	tahun	indikator_ipm	satuan	nilai
    public function sourceIpm($file)
    {
        $resp  = array();
        $fopen = fopen($file, "r");
        $src   = array();
        while($csv = fgetcsv($fopen)):
            $src[$csv[0]][$csv[2]][$csv[4]][$csv[3]] = array('nilai' => $csv[6], 'satuan' => $csv[5]);
        endwhile;
        fclose($fopen);

        if(count($src) > 1) $resp['propinsi'] = array_slice($src, 1);
        return $resp;
    }
}
