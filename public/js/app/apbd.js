'use strict';

function displayAll() {
    $('.display-all').click(function() {
        $(this).hide();
        var e = $(this).attr('display');
        $('#level-' + e + '-list').removeClass('hide');
    });
}

function apbdChart(chartId, source, ticks, series) {
    var plot1 = $.jqplot(chartId, source, {
        // The "seriesDefaults" option is an options object that will
        // be applied to all series in the chart.
        seriesDefaults: {
            renderer: $.jqplot.BarRenderer,
            highlighter: {
                showTooltip: true,
                sizeAdjust: 1,
                tooltipOffset: 9
            },
            rendererOptions: {
                fillToZero: true,
                animation: {
                    show: true,
                }
            }
        },
        cursor: {
            style: 'crosshair',
            showTooltip: true,
        },
        // Custom labels for the series are specified with the "label"
        // option on the series option.  Here a series option object
        // is specified for each series.
        series: series,
        // Show the legend and put it outside the grid, but inside the
        // plot container, shrinking the grid to accomodate the legend.
        // A value of "outside" would not shrink the grid and allow
        // the legend to overflow the container.
        legend: {
            show: true,
            placement: 'inside',
            //location: 'e',
            //placement: 'outside'
            //placement: 'outsideGrid'
        },
        axes: {
            // Use a category axis on the x axis and use our custom ticks.
            xaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                ticks: ticks
            },
            // Pad the y axis just a little so bars can get close to, but
            // not touch, the grid boundaries.  1.2 is the default padding.
            yaxis: {
                pad: 1.05,
                tickOptions: {
                    formatString: '%d'
                }
            }
        }
    });
    $('.jqplot-highlighter-tooltip').addClass('ui-corner-all');
}

function ipmChart(idChart, data, title, xAxis, yAxis){
	//var plot2 = $.jqplot ('chart2', [s], {
	var plotIpm = $.jqplot(idChart, [data], {
		title: title,
      	animate: true,
		animateReplot: true,
		cursor: {
			show: true,
			zoom: true,
			looseZoom: true,
			showTooltip: false
		},
		axesDefaults: {
			labelRenderer: $.jqplot.CanvasAxisLabelRenderer
		},
		seriesDefaults: {
          rendererOptions: {
              //smooth: true
          }
		},
		highlighter: {
			show: true, 
			showLabel: true, 
			tooltipAxes: 'y',
			sizeAdjust: 7.5 , tooltipLocation : 'ne'
		},
		axes: {
			// options for each axis are specified in seperate option objects.
			xaxis: {
				label: xAxis,
				// Turn off "padding".  This will allow data point to lie on the
				// edges of the grid.  Default padding is 1.2 and will keep all
				// points inside the bounds of the grid.
				pad: 1.2
			},
			yaxis: {
				label: yAxis,
				pad: 1.2,
				formatString: "%.d",
				tickOptions:{
					formatString:'%.2f'
				}
			}
		}
	});
	$('.jqplot-highlighter-tooltip').addClass('ui-corner-all');
}

$(function(){
	var apbdUrl = $('#level-0').attr('apbd');
	var source = $('#level-0').attr('source') ? $('#level-0').attr('source') : '/source';
	var ipmUrl = $('#level-0').attr('ipm');
	if(ipmUrl != '') $('#external-source').parent('li').addClass('active');
    var lPropinsi, lKabupaten, lAkun, lKelompok, lJenis;
	var data;
    var ipm;
	$.post(source, {apbd: apbdUrl}, function(j){
		$('#level-0').html('<div'
			+ ' class="col-md-3 no-left goid-sidebar no-outer-left" id="level-propinsi" ></div>'
			+ '<div class="col-md-9 col-md-offset-3 no-left" id="level-kabupaten"></div>');
		if(j.propinsi){
			data = j.propinsi;
			$('#level-propinsi').html('<ul class="list-group nav nav-sidebar"></ul>');
			$('#level-propinsi ul').append('<li class="list-group-item">'
				+ '<input class="form-control" placeholder="Cari Propinsi" id="q-propinsi"></li>');
			$.each(data, function(p, k){
				$('#level-propinsi ul').append('<li class="list-group-item is-hand propinsi"'
					+ ' data-propinsi="' + p + '" title="Click untuk melihat Kab/Kota ' + p + '">' + p + '</li>');
			});
			$('#level-propinsi ul').liveFilter('#q-propinsi', 'li.propinsi', {});
			$('li.propinsi').click(function(){
				var f = $(this),
					p0 = data[f.attr('data-propinsi')];
				$('li.propinsi').removeClass('active');
				f.addClass('active');
				lPropinsi = f.text();
				$('#level-kabupaten').html('<h4><i id="toggle-propinsi" style="margin-left: 10px" '
						+  'class="pull-right glyphicon glyphicon-indent-right is-hand"></i> ' 
						+ lPropinsi + '</h4>'
					+ '<div id="level-kabupaten-list">'
						+ '<ul class="list-group"></ul></div>'
					+ '<div id="level-akun" class="col-md-12 no-left no-outer-left"'
						+ ' style="padding: 0px"></div>'
                    + '<div id="level-ipm" class="col-md-12 no-left no-outer-left"'
						+ ' style="padding: 0px"></div>');
				$('#toggle-propinsi').click(function(){
					$('.goid-sidebar').toggle();
					$(this).toggleClass('glyphicon-indent-right glyphicon-indent-left');
					
				});
				$('#level-kabupaten-list ul').append('<li class="list-group-item">'
					+ '<input class="form-control" placeholder="Cari Kabupaten di '
						+ lPropinsi + '" id="q-kabupaten"></li>');
				$.each(p0, function(k0, l0){
					$('#level-kabupaten-list ul').append('<li class="list-group-item is-hand kabupaten"'
						+ ' data-kabupaten="' + k0 + '"'
						+ ' title="Click untuk melihat kategori APBD dan IPM ' + k0 + '">' + k0 + '</li>');
				});
				$('#level-kabupaten ul').liveFilter('#q-kabupaten', 'li.kabupaten', {});
				$('li.kabupaten').click(function(){
					var g = $(this),
						k0 = p0[g.attr('data-kabupaten')];
					$('li.kabupaten').removeClass('active');
					g.addClass('active');
					$('#level-kabupaten h4')
						.append('<i class="pull-right glyphicon glyphicon-align-justify display-all is-hand"'
						+ ' display="kabupaten" title="Lihat semua kabupaten di Prop ' + lPropinsi + '"></i>');
					$('#level-kabupaten-list').addClass('hide');
					displayAll();
					lKabupaten = g.text();
					$('#level-akun').html('<h4>' + lKabupaten + '</h4>'
						+ '<div id="level-akun-list">'
							+ '<ul class="list-group"></ul></div>'
						+ '<div id="level-kelompok" class="col-md-12" style="margin: 0px; padding: 0px"></div>');
                    
                    if(!ipm){
                        $('#level-ipm').html('<div style="padding: 20px; text-align: center">'
                            + '<img src="/img/creating-chart.gif"></div>');
                        $.post('/source/ipm', {ipm: ipmUrl}, function(ipm0){
                            $('#level-ipm').empty();
                            if(ipm0.propinsi && ipm0['propinsi'][lPropinsi] 
                                && ipm0['propinsi'][lPropinsi][lKabupaten]){
                                ipm = ipm0['propinsi'];
                                $('#level-ipm').html('<div class="panel panel-primary">'
                                    + '<div class="panel-heading">'
                                        + '<h3 class="panel-title">Index Pembangunan Manusia ' + lKabupaten + '</h3></div>'
                                    + '<div class="panel-body">'
                                        + '<table id="table-ipm" style="font-size: .9em"'
											+ ' class="table table-bordered table-striped table-condensed">'
                                        + '<thead><tr><th>Indicator</th></tr></thead><tbody></tbody></table>'
										+ '<div id="ipm-chart"></div></div></div>');
                                var thnLabel = [];
                                thnLabel.push('Satuan');
								var xChart = 1;
                                $.each(ipm[lPropinsi][lKabupaten], function(indikator, valIpm){
                                    var indikatorIpm = '';
									var td0 = 0;
									//Chart
									var chSrc = [];
									var yAxis;
                                    $.each(valIpm, function(tahun, nilai){
                                        if($.inArray(tahun, thnLabel) == -1) thnLabel.push(tahun);
										if(td0 == 0) indikatorIpm += '<td>' + nilai.satuan + '</td>';
                                        indikatorIpm += '<td>' + nilai.nilai + '</td>';
										chSrc.push([parseInt(tahun), nilai.nilai * 1]);
										yAxis = nilai.satuan;
										td0++;
                                    });
                                    $('#table-ipm tbody').append('<tr><td class="indikator">' + indikator + '</td>' 
                                        + indikatorIpm + '</tr>');
									$('#ipm-chart').append('<div class="col-md-6 chart-background" id="ipm-chart-' + xChart 
										+ '" style="width: 400px; height: 200px;">');
									
									ipmChart('ipm-chart-' + xChart, chSrc, indikator, 'Tahun', yAxis);
									xChart++;
                                });
                                if(thnLabel.length){
                                    $.each(thnLabel, function(thnI, thnV){
                                        $('#table-ipm thead tr').append('<th>' + thnV + '</th>'); 
                                    });
                                }
                            }
                        })
                        .fail(function(){
							ipm = false;
                            $('#level-ipm').html('<div class="alert alert-danger" role="alert">'
								+ 'Invalid raw data link or <br>'
  								+ 'Data was not processed completelly.<br>'
								+ ' Please check your source or <a href="#" class="alert-link">refresh</a>'
								+ '</div>');
                        });
                    }else{                        
                        $('#level-ipm').html('<div class="panel panel-primary">'
                            + '<div class="panel-heading">'
                                + '<h3 class="panel-title">Index Pembangunan Manusia ' + lKabupaten + '</h3></div>'
                            + '<div class="panel-body">'
                                + '<table id="table-ipm" style="font-size: .9em"'
									+ ' class="table table-bordered table-striped table-condensed">'
                                + '<thead><tr><th>Indicator</th></tr></thead><tbody></tbody></table>'
								+ '<div id="ipm-chart"></div></div></div>');
                        var thnLabel = [];
                        thnLabel.push('Satuan');
                        var xChart = 1;
                        $.each(ipm[lPropinsi][lKabupaten], function(indikator, valIpm){
                            var indikatorIpm = '';
							var td0 = 0;
							//Chart
							var chSrc = [];
							var yAxis;
                            $.each(valIpm, function(tahun, nilai){
                                if($.inArray(tahun, thnLabel) == -1) thnLabel.push(tahun);
								if(td0 == 0) indikatorIpm += '<td>' + nilai.satuan + '</td>';								
                                indikatorIpm += '<td>' + nilai.nilai + '</td>';
								chSrc.push([parseInt(tahun), nilai.nilai * 1]);
								yAxis = nilai.satuan;
								td0++;
                            });
                            $('#table-ipm tbody').append('<tr><td class="indikator">' + indikator + '</td>' 
                                + indikatorIpm + '</tr>');
							$('#ipm-chart').append('<div class="col-md-6 chart-background" id="ipm-chart-' + xChart 
								+ '" style="width: 400px; height: 200px;">');
							ipmChart('ipm-chart-' + xChart, chSrc, indikator, 'Tahun', yAxis);
							xChart++;
                        });
                        if(thnLabel.length){
                            $.each(thnLabel, function(thnI, thnV){
                                $('#table-ipm thead tr').append('<th>' + thnV + '</th>'); 
                            });
                        }
                    }
                    
					$.each(k0, function(a1, b1){
						$('#level-akun-list ul').append('<li class="list-group-item is-hand akun"'
							+ ' data-akun="' + a1 + '"'
							+ ' title="Click untuk melihat jenis ' + a1 + ' APBD ' + lKabupaten + '">' + a1 + '</li>');
					});
					$('.akun').click(function(){
						var h = $(this),
							a0 = k0[h.attr('data-akun')];
						lAkun = h.text();
						$('li.akun').removeClass('active');
						h.addClass('active');
						$('#level-akun h4').append('<i class="pull-right'
						 	+ ' glyphicon glyphicon-align-justify display-all is-hand"'
								+ ' display="akun" title="Lihat semua kategori APBD ' + lKabupaten + '"></i>');
						$('#level-akun-list').addClass('hide');
						displayAll();
						$('#level-kelompok').html('<h4>' + lAkun + '</h4>'
							+ '<div id="level-kelompok-list"><ul class="list-group"></ul></div>'
							+ '<div id="level-jenis" class="col-md-12" style="margin: 0px; padding: 0px"></div>');
						$.each(a0, function(k2, l2){
							$('#level-kelompok-list ul')
								.append('<li class="list-group-item is-hand kelompok"'
								 + ' data-kelompok="' + k2 + '"'
								 + ' title="Click untuk melihat jenis ' + k2 + ' ' + lKabupaten + '">' + k2 + '</li>');
						});
						$('.kelompok').click(function(){
							var u = $(this),
								j0 = a0[u.attr('data-kelompok')];
							lKelompok = u.text();
							$('#level-kelompok h4').append('<i class="pull-right'
							 	+ ' glyphicon glyphicon-align-justify display-all is-hand"'
								+ ' display="kelompok" title="Lihat semua kelompok ' + lAkun + ' APBD"></i>');
							$('#level-kelompok-list').addClass('hide');
							displayAll();

							$('li.kelompok').removeClass('active');
							u.addClass('active');
							$('#level-jenis').html('<h4>' + lKelompok + '</h4>'
								+ '<div id="level-jenis-list">'
									+ '<ul class="list-group"></ul></div>'
								+ '<div id="level-tahun" class="col-md-12" style="margin: 0px; padding: 0px"></div>');
							var xRef = 1;
							$.each(j0, function(j3, k3){
								$('#level-jenis-list ul')
									.append('<li class="list-group-item">'
										+ '<i class="glyphicon glyphicon-map-marker is-hand jenis"'
										+ ' data-jenis="' + j3 + '"'
										+ ' title="Click untuk melihat ' + j3 + ' per tahun ' + lKabupaten + '"></i> ' + j3
										+ '<input type="checkbox" class="pull-right add-jenis"'
											+ ' disabled value="' + j3 + '" x-ref="' + xRef + '"'
											+ ' title="Check untuk membandingkan dengan ' + lKelompok + ' yang lain"></li>');
								xRef++;
							});

							$('.jenis').click(function(){
								var w = $(this),
									wp = w.parent('li'),
									t0 = j0[w.attr('data-jenis')];
								lJenis = wp.text();
								$('input.add-jenis').prop('checked', false).prop('disabled', false);
								wp.children('input.add-jenis').prop('disabled', true).prop('checked', true);
								$('#level-jenis h4').append('<i class="pull-right'
								 	+ ' glyphicon glyphicon-align-justify display-all is-hand"'
									+ ' display="jenis"  title="Lihat semua jenis APBD dari ' + lKelompok + '"></i>');
								$('#level-jenis-list').addClass('hide');
								displayAll();

								wp.parent('ul').children('li').removeClass('active');
								wp.addClass('active');
								$('#level-tahun').html('<div class="col-md-6 no-left no-outer-left"'
									+ ' id="level-tahun-list"><h4 class="reference-0">' + lJenis + '</h4>'
											+ '<ul class="list-group apbd"></ul></div>'
											+ '<div id="level-chart" class="col-md-12" style="margin: 0px; padding: 0px">'
											+ '</div>');
								$.each(t0, function(t4, v4){
									var m0 = accounting.formatMoney(v4, "Rp ", 0, ".", ","); // €4.999,99
									$('#level-tahun-list ul').append('<li class="list-group-item">' + t4
										+ '<input class="pull-right tahun tahun-0" style="margin-left: 10px"'
										+ ' value="' + v4 + '" title="' + t4 + '" type="checkbox" alt="0">'
										+ '<span class="pull-right">' + m0 + '</span></li>');
								});

								var multiLabel = [];
								multiLabel.push({});
								multiLabel[0].label = $('#level-tahun-list h4.reference-0').text();
								$('.tahun').click(function(){
									var d4 = [];
									var l4 = [];
									$.each($('input.tahun:checked'), function(e4, f4){
										d4.push(parseInt(f4.value));
										l4.push(f4.title);
									});
									var g0 = [];
									if(d4.length){
										var lChart = lKabupaten + ' / ' + lAkun + ' / ' + lKelompok;
										$('#level-chart').html('<div class="panel panel-primary">'
					  						+ '<div class="panel-heading"><h3 class="panel-title">' + lChart + '</h3>'
											+ '</div><div class="panel-body chart-background">'
											+ '<div id="apbd-chart" style="width: 800px; height: 300px;"></div></div></div>');
										g0.push(d4);
										var ix = 1;
										$.each($('.compare'), function(s, t){
											var ref0 = t.value;
											g0.push([]);
											multiLabel.push({});
											multiLabel[ix].label = t.title;
											$.each($('.apbd-' + ref0 + ' input.tahun-add'), function(ss, tt){
												if($.inArray(tt.title, l4) != -1){
													g0[ix].push(parseInt(tt.value));
												}
											});
											ix++;
										});

										$('#apbd-chart').empty();
										apbdChart('apbd-chart', g0, l4, multiLabel);
									}
								});
							});
							$('input.add-jenis').click(function(){
								var cx = $(this),
									px = cx.parent('li'),
									rx = cx.attr('x-ref'),
									vx = cx.val();
								var xJenis = px.text();
								var dx4 = [], lx4 = [];
								var multi = [];
								var multiLabel = [];
								multiLabel.push({});
								multiLabel[0].label = $('#level-tahun-list h4.reference-0').text();
								$.each($('input.tahun-0:checked'), function(e4, f4){
									dx4.push(parseInt(f4.value));
									lx4.push(f4.title);
								});
								multi.push(dx4);
								if(cx.is(':checked')){
									var dx = j0[vx];
									if($('#level-jenis-list-' + rx).length == 0){
										$('#level-chart').before('<div id="level-jenis-list-' + rx
											+ '" class="col-md-6 no-left no-outer-left"></div>');
									}
									$('#level-jenis-list-' + rx)
										.append('<h4><input type="hidden" class="compare" value="' + rx + '"'
											+ ' title="' + xJenis + '">'
											+ '<i class="is-hand glyphicon glyphicon-remove-sign rm-jenis" x-ref="' + rx + '"></i>'
											+ xJenis + '</h4><ul class="list-group apbd-' + rx + '"></ul>');
									$.each(dx, function(tx, tv){
										$('#level-jenis-list-' + rx + ' ul')
											.append('<li class="list-group-item">'
												+ tx + '<span class="pull-right">'
												+ accounting.formatMoney(tv, "Rp ", 0, ".", ",") + '</span>'
												+ '<input class="tahun-add" type="hidden" alt="' + rx + '"'
												+ ' value="' + tv + '" title="' + tx + '"></li>');
									});
									var ix = 1;
									$.each($('.compare'), function(s, t){
										var ref0 = t.value;
										multi.push([]);
										multiLabel.push({});
										multiLabel[ix].label = t.title;
										$.each($('.apbd-' + ref0 + ' input.tahun-add'), function(ss, tt){
											if($.inArray(tt.title, lx4) != -1){
												multi[ix].push(parseInt(tt.value));
											}
										});
										ix++;
									});
									$('.rm-jenis').click(function(){
										var rmx = $(this),
											rmv = rmx.attr('x-ref');
										$('input.add-jenis[x-ref="' + rmv + '"]').prop('checked', false);
										$('#level-jenis-list-' + rmv).empty();
										var ix = 1;
										var multi = [];
										multi.push(dx4);
										var multiLabel = [];
										multiLabel.push({});
										multiLabel[0].label = $('#level-tahun-list h4.reference-0').text();
										$.each($('.compare'), function(s, t){
											var ref0 = t.value;
											multi.push([]);
											multiLabel.push({});
											multiLabel[ix].label = t.title;
											$.each($('.apbd-' + ref0 + ' input.tahun-add'), function(ss, tt){
												if($.inArray(tt.title, lx4) != -1){
													multi[ix].push(parseInt(tt.value));
												}
											});
											ix++;
										});
										if(multi[0].length){
											var lChart = lKabupaten + ' / ' + lAkun + ' / ' + lKelompok;
											$('#level-chart').html('<div class="panel panel-primary">'
												+ '<div class="panel-heading"><h3 class="panel-title">' + lChart + '</h3>'
												+ '</div><div class="panel-body chart-background">'
												+ '<div id="apbd-chart" style="width: 800px; height: 300px;"></div></div></div>');
												$('#apbd-chart').empty();
												apbdChart('apbd-chart', multi, lx4, multiLabel);
										}
									});
								}else{
									$('#level-jenis-list-' + rx).empty();
									var ix = 1;
									var multiLabel = [];
									multiLabel.push({});
									multiLabel[0].label = $('#level-tahun-list h4.reference-0').text();
									$.each($('.compare'), function(s, t){
										var ref0 = t.value;
										multi.push([]);
										multiLabel.push({});
										multiLabel[ix].label = t.title;
										$.each($('.apbd-' + ref0 + ' input.tahun-add'), function(ss, tt){
											if($.inArray(tt.title, lx4) != -1){
												multi[ix].push(parseInt(tt.value));
											}
										});
										ix++;
									});
								}
								if(multi[0].length){
									var lChart = lKabupaten + ' / ' + lAkun + ' / ' + lKelompok;
									$('#level-chart').html('<div class="panel panel-primary">'
										+ '<div class="panel-heading"><h3 class="panel-title">' + lChart + '</h3>'
										+ '</div><div class="panel-body chart-background">'
										+ '<div id="apbd-chart" style="width: 800px; height: 300px;"></div></div></div>');
										$('#apbd-chart').empty();
										apbdChart('apbd-chart', multi, lx4, multiLabel);

								}
							});
						});
					});
				});
			});
		}
	})
	.done(function(){

	})
	.fail(function(){
		$('#level-0').html('<div class="alert alert-danger" role="alert">'
  			+ 'Data was not processed completelly. Please <a href="#" class="alert-link">refresh</a>'
		+ '</div>');
	});
});
