'use strict';
$(function(){
	var img = [];
	img.push({img: 'padsemarang.png', title: 'PAD Kota Semarang'});
	img.push({img: 'danaperimbanganbandung.png', title: 'Dana Perimbangan Kota Bandung'});
	img.push({img: 'pendapatanlainsurakarta.png', title: 'Lain Lain Pendapatan Kota Surakarta'});
	img.push({img: 'belanjalangsungsurabaya.png', title: 'Belanja Langsung Kota Surabaya'});
	img.push({img: 'belanjalangsungyogyakarta.png', title: 'Belanja Langsung Kota Yogyakarta'});
	img.push({img: 'belanjatidaklangsungbatam.png', title: 'Belanja Tidak Langsung Kota Batam'});
	img.push({img: 'ipmpekanbaru.png', title: 'IPM Kota Pekanbaru'});
	img.push({img: 'penerimaanpembiayaanpati.png', title: 'Penerimaan Pembiayaan Kab Pati'});
	img.push({img: 'pengeluaranpembiayaanmalang.png', title: 'Pengeluaran Pembiayaan Kota Malang'});
	img.push({img: 'databerjenjang.png', title: 'Data Berjenjang'});
	var primary  = '<div  style="margin-left: auto; margin-right: auto; text-align: center; margin-bottom: 10px">'
		+ '<img src="img/presentasi.jpg" class="img-rounded"></div>';
	var carousel = '<div id="slideshow" class="carousel slide" data-ride="carousel">'
  		+ '<ol class="carousel-indicators">'
	for(var i = 0; i < img.length; i++){
		var olActive = i == 0 ? 'active' : '';
		carousel += '<li data-target="#slideshow" data-slide-to="' + i + '" class="' + olActive + '"></li>';
	}
  	carousel += '</ol><div class="carousel-inner" role="listbox">';
	$.each(img, function(u, x){
		var isActive = u == 0 ? 'active' : '';
		carousel += '<div class="item ' + isActive + '" style="text-align: center">'
			+ '<div style="text-align: center"><img src="img/chart/' + x.img + '" alt="' + x.title + '">'
			+ '<div class="carousel-caption" style="color: blue">' + x.title + '</div></div></div>';
	});
  	carousel += '</div>'
  			+ '<a class="left carousel-control" href="#slideshow" role="button" data-slide="prev">'
    		+ '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>'
    		+ '<span class="sr-only">Previous</span>'
  		+ '</a>'
  		+ '<a class="right carousel-control" href="#slideshow" role="button" data-slide="next">'
    		+ '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'
    		+ '<span class="sr-only">Next</span>'
  		+ '</a>'
		+ '</div>';
	$('.page').click(function(el){
		el.preventDefault();
		$('#goid-modal').html('<div class="modal-dialog  modal-lg" role="document">'
			+ '<div class="modal-content">'
				+ '<div class="modal-header">'
					+ '<i data-dismiss="modal" aria-label="Close" class="close glyphicon glyphicon-remove-sign"></i>'
					+ '<h4 class="modal-title">Tentang ' + $('#nav-home').text() + '</h4>'
					+ '<p>Konversi data tabel menjadi data berjenjang dan chart</p>'
				+ '</div>' 
			+ '<div class="modal-body">' + primary + carousel + '</div>'
			+ '</div></div>');
		$('#goid-modal').modal('show');
	})
})
