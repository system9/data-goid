<?php

class Dashboard_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        parent::init();
        $this->view->headScript()->appendFile($this->view->baseUrl('js/jqplot/jquery.jqplot.min.js'));
        $this->view->headScript()
            ->appendFile($this->view->baseUrl('js/jqplot/excanvas.min.js'), 'text/javascript',
                        array('conditional' => 'lt IE 9'));
        $this->view->headScript()->appendFile($this->view->baseUrl('js/plugins/jquery.liveFilter.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('js/plugins/accounting.min.js'));
        $chPlugins[] = 'jqplot.barRenderer.min.js';
        $chPlugins[] = 'jqplot.categoryAxisRenderer.min.js';
        $chPlugins[] = 'jqplot.canvasTextRenderer.min.js';
        $chPlugins[] = 'jqplot.canvasAxisLabelRenderer.min.js';
        $chPlugins[] = 'jqplot.cursor.min.js';
        $chPlugins[] = 'jqplot.pointLabels.min.js';
        $chPlugins[] = 'jqplot.highlighter.min.js';
        foreach($chPlugins as $plug)
            $this->view->headScript()->appendFile($this->view->baseUrl('js/jqplot/plugins/' . $plug));
        $this->view->headLink()
            ->appendStylesheet($this->view->baseUrl('js/jqplot/jquery.jqplot.min.css'));
    }

    public function indexAction()
    {
        $apbd   = $this->_getParam('apbd', false);
        $source = $this->_getParam('source', false);
        $ipm    = $this->_getParam('ipm', false);
        if($apbd) $this->view->apbd = $apbd;
        if($source) $this->view->source = $source;
        if($ipm) $this->view->ipm = $ipm;
        $this->view->headScript()->appendFile($this->view->baseUrl('js/app/apbd.js'));
    }


}
