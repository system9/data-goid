<?php

class Dashboard_SourceController extends Zend_Controller_Action
{

    public function init()
    {
        parent::init();
        $this->_helper->ViewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
    }

    public function indexAction()
    {
        $file = $this->_getFullUrl() . "/file/data-apbd.csv";
        if($this->_getParam('apbd', false))
            $file = $this->_getParam('apbd');
        $source = new Goid_Source();
        $this->_helper->json($source->sourceApbd($file));
    }

    public function ipmAction()
    {
        $file = $this->_getFullUrl() . "/file/data-ipm.csv";
        if($this->_getParam('ipm', false))
            $file = $this->_getParam('ipm');
        $source = new Goid_Source();
        $this->_helper->json($source->sourceIpm($file));
    }

    protected function _getFullUrl()
    {
        return
            (isset($_SERVER['HTTPS']) ? 'https://' : 'http://').
            (isset($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'].'@' : '').
            (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ($_SERVER['SERVER_NAME'].
            (isset($_SERVER['HTTPS']) && $_SERVER['SERVER_PORT'] === 443 ||
            $_SERVER['SERVER_PORT'] === 80 ? '' : ':'.$_SERVER['SERVER_PORT']))).
            substr($_SERVER['SCRIPT_NAME'],0, strrpos($_SERVER['SCRIPT_NAME'], '/'));
    }
}
