<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    public function _initModuleLoaders()
    {
        $this->bootstrap('Frontcontroller');

        $fc0 = $this->getResource('Frontcontroller');
        $modules = $fc0->getControllerDirectory();

        foreach ($modules as $module => $dir) {
          $moduleName = strtolower($module);
          $moduleName = str_replace(array('-', '.'), ' ', $moduleName);
          $moduleName = ucwords($moduleName);
          $moduleName = str_replace(' ', '', $moduleName);

          $loader = new Zend_Application_Module_Autoloader(array(
              'namespace' => $moduleName,
              'basePath' => realpath($dir . "/../"),
          ));
        }
    }

    protected function _initView()
    {
        $view = new Zend_View();
        $view->doctype('XHTML1_STRICT');
        $view->headTitle('data.go.id')->setSeparator(' - ');
        $view->headMeta()
                ->appendHttpEquiv('Expires', gmdate('D, d M Y H:i:s', time( ) + 10800) . ' GMT')
                ->appendHttpEquiv('Cache-Control', 'must-revalidate')
                ->appendHttpEquiv('Content-Type', 'application/xhtml+xml; charset=utf-8')
                ->setName('dicoding:email', 'terra.orion@gmail.com')
                ->setName('description', 'Visualization Graphis')
                ->setName('keywords', 'data.go.id')
                ->setName('robots', 'index, follow')
                ->setName('viewport', 'width=device-width, initial-scale=1.0');
        $view->headLink()->appendStylesheet($view->baseUrl('css/bootstrap.min.css'));
        $view->headLink()->appendStylesheet($view->baseUrl('css/bootstrap-theme.min.css'));
        $view->headLink()->appendStylesheet($view->baseUrl('css/app.css'));
        $view->headLink()->appendStylesheet($view->baseUrl('css/animate.css'));
        $view->headScript()->prependFile($view->baseUrl('js/jquery-1.10.2.min.js'));
        $view->headScript()->appendFile($view->baseUrl('js/bootstrap.min.js'));
        $view->headScript()->appendFile($view->baseUrl('js/noty/jquery.noty.packaged.min.js'));
        $view->headScript()->appendFile($view->baseUrl('js/app/general.js'));

        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
        $viewRenderer->setView($view);
        return $view;
    }

}
